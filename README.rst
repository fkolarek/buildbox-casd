What is ``buildbox-casd``?
==========================

``buildbox-casd`` is a long-lived local cache and proxy for the
``ContentAddressableStorage`` services described by Bazel's
`Remote Execution API`_. Its intended purpose is to run on the same host
as a remote worker or client to provide fast access to data in CAS.

.. _Remote Execution API: https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s

It also supports the `LocalCAS Protocol`_.

.. _LocalCAS Protocol: https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto


Installation
============

``buildbox-casd`` requires only `buildbox-common`_ as a dependency. You can install it anywhere
on your system. If you install it system-wide, you can omit the ``-DBuildboxCommon_DIR`` flag
below.

Once you have installed ``buildbox-common`` somewhere, you can build ``buildbox-casd`` as follows:

.. code-block:: sh

    git clone https://gitlab.com/BuildGrid/buildbox/buildbox-casd.git && cd buildbox-casd
    mkdir build && cd build
    cmake -DBuildboxCommon_DIR=/path/to/installation/usr/local/lib/cmake/BuildboxCommon/ ..
    make

You can then install it in the current environment with

.. code-block:: sh

    make install

.. _buildbox-common: https://gitlab.com/BuildGrid/buildbox/buildbox-common


Usage
=====

``buildbox-casd`` can be configured in two modes:

* As a regular CAS server that stores blobs on disk. In this mode, ``buildbox-casd`` is the source of truth for all blobs in CAS.

* As a CAS proxy. In this mode ``buildbox-casd`` is pointed to a remote CAS server, and it will forward all requests to it while caching data locally.

   buildbox-casd [OPTIONS] LOCAL_CACHE_PATH

This starts a daemon storing local cached data at the ``LOCAL_CACHE_PATH`` path.

The key parameters are:

* ``--cas-remote=URL``

  URL of the remote CAS server to which requests should be forwarded to.
  Setting this option configures ``buildbox-casd`` to act in the proxy mode.

  If this is not provided, ``buildbox-casd`` behaves like a regular CAS server storing blobs on disk.

* ``--ra-remote=URL``

  URL of the Remote Asset server to which requests should be forwarded to
  Setting this option configures ``buildbox-casd`` to act in the proxy mode.

  If this is not provided, ``buildbox-casd`` behaves like a regular Remote Asset server storing blobs on disk.

* ``--instance=NAME``

  Name given to the new LocalCAS instance (if not set, an empty string).

* ``--verbose``

  Output additional information to stdout that may be useful when debugging.

* ``--bind=URL``

  Endpoint where ``casd`` listens for incoming requests.

  By default a Unix domain socket in ``LOCAL_CACHE_PATH/casd.sock``.

* ``--metrics-mode=MODE``

  Where mode is one of the following: ``udp://<hostname>:<port>``, ``file:///path/to/file``, or ``stderr``

* ``--metrics-publish-interval=VALUE``

  Where VALUE is the number of seconds that the background thread sleeps in between publishing metrics


Full usage
----------
::

    usage: ./buildbox-casd [OPTIONS] LOCAL_CACHE_PATH
        --instance=NAME             LocalCAS instance name (default "")
        --cas-remote=URL            URL for CAS service
        --cas-instance=NAME         Name of the CAS instance
        --cas-server-cert=PATH      Public server certificate for TLS (PEM-encoded)
        --cas-client-key=PATH       Private client key for TLS (PEM-encoded)
        --cas-client-cert=PATH      Public client certificate for TLS (PEM-encoded)
        --cas-access-token=PATH     Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token.
        --cas-googleapi-auth        Use GoogleAPIAuth when this flag is set.
        --cas-retry-limit=INT       Number of times to retry on grpc errors
        --cas-retry-delay=MILLISECONDS How long to wait before the first grpc retry
        --ra-remote=URL             URL for Remote Asset service
        --ra-instance=NAME          Name of the Remote Asset instance
        --ra-server-cert=PATH       Public server certificate for TLS (PEM-encoded)
        --ra-client-key=PATH        Private client key for TLS (PEM-encoded)
        --ra-client-cert=PATH       Public client certificate for TLS (PEM-encoded)
        --ra-access-token=PATH      Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token.
        --ra-googleapi-auth         Use GoogleAPIAuth when this flag is set.
        --ra-retry-limit=INT        Number of times to retry on grpc errors
        --ra-retry-delay=MILLISECONDS How long to wait before the first grpc retry
        --bind                      Bind to "address:port" or UNIX socket in
                                    "unix:path".
                                    (Default: unix:LOCAL_CACHE_PATH/casd.sock)
        --quota-high=SIZE           Maximum local cache size (e.g., 50G or 2T)
        --quota-low=SIZE            Local cache size to retain on LRU expiry
                                    (Default: Half of high quota)
        --reserved=SIZE             Reserved disk space (headroom left when `quota_high`
                                    gets automatically reduced)
                                    (Default: 2G)
        --protect-session-blobs     Do not expire blobs created or used
                                    in the current session
        --findmissingblobs-cache-ttl=SECS  Cache the results of FindMissingBlobs() calls
                                           when in proxy mode (Default: 0, disabled)
        --log-level=LEVEL           "trace", "debug", "info", "warning", "error" (Default: "error").
        --metrics-mode=MODE   Options for MODE are:
               udp://localhost:50051
               file:///tmp
               stderr
                              Only one metric output mode can be specified
        --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds
        --verbose                   Set log level to DEBUG
        --version                   Print version information and exit
        --help                      Print this text and exit


**Note**: URLs can be of the form ``http(s)://address:port``, ``unix:path`` (with a relative or absolute path), or ``unix://path`` (with an absolute path).

Staging and Capturing
=====================

As mentioned in the introduction, ``buildbox-casd`` supports the `LocalCAS Protocol`_.
This allows you to put files and directories from the remote filesystem onto
your local filesystem (known as "Staging") and snapshot files or a directory structure
for placement into CAS (known as "Capturing"). Using this protocol allows you to
effectively use buildbox-casd as a local filesystem cache for your remote CAS.

`buildbox-worker`_ supports communication with this protocol. To enable usage, pass
:code:`--runner-arg=--use-localcas` on the **buildbox-worker** commandline.

The stage/capture mechanism can be used in two modes: with FUSE and with hardlinks.
The FUSE mode requires `buildbox-fuse`_ to be present on the system, NodeProperties are
supported, and writing to the input files is generally safe. The hardlinks mode does not
require the ``buildbox-fuse`` tool, but NodeProperties are not supported, and writing to
staged files within actions is unsafe since future actions will read the modified files.
FUSE is enabled by default, and if ``buildbox-fuse`` is not on the system or FUSE is not
supported on the system, ``buildbox-casd`` will use hardlinks.

**For staging and capturing to be completely safe in hardlinks mode, please run**
**buildbox-casd as a different user from the client.** If you run them as the same user,
actions sent to the worker in hardlinks mode may corrupt the staged files.

.. _buildbox-worker: https://gitlab.com/BuildGrid/buildbox/buildbox-worker

.. _LocalCAS Protocol: https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto

.. _buildbox-fuse: https://gitlab.com/BuildGrid/buildbox/buildbox-fuse

Metrics
=======
`buildbox-casd` publishes `statsd`_  runtime metrics throughout its lifetime. Documentation listing the names and descriptions of these published metrics can be found with the `MetricName`_ definitions.

.. _statsd: https://github.com/statsd/statsd/blob/master/docs/metric_types.md
.. _MetricName: https://gitlab.com/BuildGrid/buildbox/buildbox-casd/-/blob/master/buildboxcasd/buildboxcasd_metricnames.cpp

Metrics Enablement
------------------
To enable metrics publishing, two command line options are required:
::

   --metrics-mode=MODE                Options for MODE are:
           udp://<hostname>:<port>
           file:///path/to/file
           stderr

   --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds

Example #1: Write metrics to a statsd server on the local host listening on port 50051 and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=udp://localhost:50051 --metrics-publish-interval=5

Example #2: Write metrics to stderr and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=stderr --metrics-publish-interval=5

Example #3: Write metrics to a file and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=file:///tmp/my-metrics.log --metrics-publish-interval=5

// Copyright 2020 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcasd_metricnames.h>

namespace buildboxcasd {
// Each metric is recorded in code with a statsd string. The statsd strings for
// each named metric are defined here. Only the constants are used in code, not
// the metric strings directly.

// LRU
// Total bytes on local disk being consumed by blobs
const std::string MetricNames::GAUGE_NAME_DISK_USAGE =
    "buildboxcasd.localcas_disk_usage";

// Elapsed wall time of disk cleanup operations
const std::string MetricNames::TIMER_NAME_LRU_CLEANUP =
    "buildboxcasd.localcas_lru_cleanup";

// Runtime-calculated high watermark in bytes
const std::string MetricNames::GAUGE_NAME_LRU_EFFECTIVE_HIGH_WATERMARK =
    "buildboxcasd.localcas_lru_effective_highwatermark";

// Runtime-calculated low watermark in bytes
const std::string MetricNames::GAUGE_NAME_LRU_EFFECTIVE_LOW_WATERMARK =
    "buildboxcasd.localcas_lru_effective_lowwatermark";

// Configured high watermark in bytes
const std::string MetricNames::GAUGE_NAME_LRU_CONFIGURED_HIGH_WATERMARK =
    "buildboxcasd.localcas_lru_configured_highwatermark";

// Configured low watermark in bytes
const std::string MetricNames::GAUGE_NAME_LRU_CONFIGURED_LOW_WATERMARK =
    "buildboxcasd.localcas_lru_configured_lowwatermark";

// CAS

// Elapsed wall time of Bytestream read operations
const std::string MetricNames::TIMER_NAME_CAS_BYTESTREAM_READ =
    "buildboxcasd.cas_bytestream_read";

// Elapsed wall time of Bytestream write operations
const std::string MetricNames::TIMER_NAME_CAS_BYTESTREAM_WRITE =
    "buildboxcasd.cas_bytestream_write";

// Elapsed wall time of a get capabilities request.
const std::string MetricNames::TIMER_NAME_CAS_GET_CAPABILITIES =
    "buildboxcasd.cas_get_capabilities";

// Elapsed wall time of remote FindMissingBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_FIND_MISSING_BLOBS =
    "buildboxcasd.cas_find_missing_blobs";

// Elapsed wall time of remote BatchUpdateBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_BATCH_UPDATE_BLOBS =
    "buildboxcasd.cas_batch_update_blobs";

// Elapsed wall time of remote BatchReadBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_BATCH_READ_BLOBS =
    "buildboxcasd.cas_batch_read_blobs";

// Elapsed wall time of remote GetTree operations
const std::string MetricNames::TIMER_NAME_CAS_GET_TREE =
    "buildboxcasd.cas_get_tree";

// Running total number of blobs queried by `FindMissingBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_FIND_MISSING =
    "buildboxcasd.localcas_num_blobs_find_missing";

// Running total number of blobs uploaded with `BatchUpdateBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_BATCH_UPDATE =
    "buildboxcasd.localcas_num_blobs_batch_update";

// Running total of blobs requested with `BatchReadBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_BATCH_READ =
    "buildboxcasd.localcas_num_blobs_batch_read";

// LOCAL CAS
// Running total of number of blobs read from local storage
const std::string MetricNames::COUNTER_NUM_BLOBS_READ_FROM_LOCAL =
    "buildboxcasd.localcas_num_blobs_read_from_local";

// Running total of number of blobs read from local storage
const std::string MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_LOCAL =
    "buildboxcasd.localcas_num_blobs_written_to_local";

// Running total of number of blobs read from remote storage
const std::string MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE =
    "buildboxcasd.localcas_num_blobs_read_from_remote";

// Elapsed wall time of local FindMissingBlobs operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_MISSING_BLOB =
    "buildboxcasd.localcas_fetch_missing_blob";

// Elapsed wall time of local UploadMissingBlobs operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_MISSING_BLOB =
    "buildboxcasd.localcas_upload_missing_blob";

// Elapsed wall time of local FetchTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_TREE =
    "buildboxcasd.localcas_fetch_tree";

// Elapsed wall time of local UploadTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_TREE =
    "buildboxcasd.localcas_upload_tree";

// Elapsed wall time of local StageTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_TOTAL =
    "buildboxcasd.localcas_stage_tree_total";

// Elapsed wall time of just the prepare tree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_PREPARE =
    "buildboxcasd.localcas_stage_tree_prepare";

// Elapsed wall time of just the staging operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_STAGE =
    "buildboxcasd.localcas_stage_tree_stage";

// Elapsed wall time of local CaptureTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_TREE =
    "buildboxcasd.localcas_capture_tree";

// Elapsed wall time of local CaptureFiles operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_FILES =
    "buildboxcasd.localcas_capture_files";

// Elapsed wall time of local GetInstanceNameForRemote operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_GET_INSTANCE_FROM_REMOTE =
    "buildboxcasd.localcas_get_instance_from_remote";

// Elapsed wall time of local GetLocalDiskUsage operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_GET_LOCAL_DISK_USAGE =
    "buildboxcasd.localcas_get_local_disk_usage";

// Running total of number of blobs written to remote storage
const std::string MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE =
    "buildboxcasd.localcas_num_blobs_written_to_remote";

// Number of bytes read from casd's local CAS storage
const std::string MetricNames::COUNTER_NAME_BYTES_READ =
    "buildboxcasd.localcas_bytes_read";

// Number of bytes written to casd's local CAS storage
const std::string MetricNames::COUNTER_NAME_BYTES_WRITE =
    "buildboxcasd.localcas_bytes_write";

// Number of bytes read from a remote CAS when configured as a proxy
const std::string MetricNames::COUNTER_NAME_REMOTE_BYTES_READ =
    "buildboxcasd.localcas_remote_bytes_read";

// Number of bytes written to a remote CAS when configured as a proxy
const std::string MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE =
    "buildboxcasd.localcas_remote_bytes_write";

// Number of files staged
const std::string MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_FILES =
    "buildboxcasd.localcas_num_hardlink_stager_files_staged";

// Number of directories staged
const std::string MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_DIRECTORIES =
    "buildboxcasd.localcas_num_hardlink_stager_directories_staged";

// Number of symlinks staged
const std::string MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_SYMLINKS =
    "buildboxcasd.localcas_num_hardlink_stager_symlinks_staged";

// Number of blobs updated via capture requests
const std::string MetricNames::COUNTER_NUM_BLOBS_CAPTURED =
    "buildboxcasd.localcas_num_blobs_captured";

// Number of `FetchTree()` requests issued:
const std::string MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_REQUESTS =
    "buildboxcasd.localcas_num_fetch_tree_requests";

// Number of `FetchTree()` requests serviced from cache (without scanning
// local storage):
const std::string
    MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_CACHE_HITS =
        "buildboxcasd.localcas_num_fetch_tree_cache_hits";

} // namespace buildboxcasd

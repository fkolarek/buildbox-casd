/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcasproxyinstance.h>
#include <buildboxcasd_metricnames.h>

#include <buildboxcommon_client.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommonmetrics_countingmetricutil.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unordered_set>

using namespace buildboxcasd;
using namespace buildboxcommon;

LocalCasProxyInstance::LocalCasProxyInstance(
    std::shared_ptr<LocalCas> storage, std::shared_ptr<FileStager> file_stager,
    const buildboxcommon::ConnectionOptions &cas_endpoint,
    const std::string &instance_name, const int findmissingblobs_cache_ttl)
    : LocalCasInstance(storage, file_stager, instance_name),
      d_cas_client(std::make_unique<Client>()),
      d_find_missing_blobs_cache(
          findmissingblobs_cache_ttl > 0
              ? std::make_unique<DigestCache>(findmissingblobs_cache_ttl)
              : nullptr)
{
    d_cas_client->init(cas_endpoint);

    d_find_missing_blobs_client = std::make_unique<FindMissingBlobsClient>(
        *d_cas_client, d_find_missing_blobs_cache.get());

    if (d_find_missing_blobs_cache) {
        BUILDBOX_LOG_INFO("Caching FindMissingBlobs() results for "
                          << findmissingblobs_cache_ttl << " seconds");
    }
}

grpc::Status
LocalCasProxyInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                        FindMissingBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    const std::vector<Digest> requested_digests(
        request.blob_digests().cbegin(), request.blob_digests().cend());

    std::vector<Digest> digests_missing_in_remote;
    const auto find_missing_blobs_status = findMissingBlobsInRemoteCas(
        requested_digests, &digests_missing_in_remote);
    if (!find_missing_blobs_status.ok()) {
        return find_missing_blobs_status;
    }

    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    CounterType bytes_uploaded = 0;
    CounterType blobs_uploaded = 0;
    for (const Digest &digest : digests_missing_in_remote) {
        if (d_storage->hasBlob(digest)) {
            // If we have the blob stored locally, we implicitly update the
            // remote.
            std::string path = d_storage->pathUnchecked(digest);
            upload_requests.emplace_back(
                buildboxcommon::Client::UploadRequest::from_path(digest,
                                                                 path));
            bytes_uploaded += digest.size_bytes();
            blobs_uploaded++;
        }
        else {
            Digest *entry = response->add_missing_blob_digests();
            entry->CopyFrom(digest);
        }
    }

    const std::vector<Client::UploadResult> not_uploaded_blobs =
        d_cas_client->uploadBlobs(upload_requests);

    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        BUILDBOX_LOG_ERROR("Error uploading "
                           << toString(upload_result.digest) << ": "
                           << upload_result.status.error_code());

        Digest *entry = response->add_missing_blob_digests();
        entry->CopyFrom(upload_result.digest);

        // Unable to upload this blob for some reason, subtract it from
        // the uploaded bytes count
        bytes_uploaded -= upload_result.digest.size_bytes();
        blobs_uploaded--;
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_READ_FROM_LOCAL,
                            upload_requests.size());
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                            bytes_uploaded);
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE,
                            blobs_uploaded);

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                        BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    for (const auto &blob : request.requests()) {
        writeToLocalStorage(blob.digest(), blob.data());
    }

    // We assume that the client called `FindMissingBlobs()` previous to
    // this call, so we will upload all the blobs in the request.
    const std::unordered_map<Digest, grpc::Status> digests_not_uploaded =
        batchUpdateRemoteCas(request);

    for (const auto &blob : request.requests()) {
        google::rpc::Status status;

        const auto failed_upload_it = digests_not_uploaded.find(blob.digest());
        if (failed_upload_it != digests_not_uploaded.cend()) {
            status.set_code(failed_upload_it->second.error_code());
            status.set_message(failed_upload_it->second.error_message());
        }
        else {
            status.set_code(grpc::StatusCode::OK);

            // We assume that the remote will keep the blob for a while, so
            // we'll avoid querying for it in subsequent `FindMissingBlobs()`:
            addToFindMissingBlobsCache(blob.digest());
        }

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                      BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status read_status =
            readFromLocalStorage(digest, &data);

        if (read_status.code() == grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(read_status);
            entry->set_data(data);
        }
        else {
            digests_missing_locally.push_back(digest);
        }
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_BATCH_READ,
                            request.digests().size());

    if (digests_missing_locally.empty()) {
        BUILDBOX_LOG_INFO("retrieved all digest(s) for instance name \""
                          << request.instance_name()
                          << "\" from local storage");
        return grpc::Status::OK;
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadBlobsResult downloaded_data =
        d_cas_client->downloadBlobs(digests_missing_locally);

    size_t count = 0;
    CounterType bytes_downloaded = 0;
    for (const auto &digest : digests_missing_locally) {
        const auto &download_entry = downloaded_data.at(digest.hash());
        const auto &download_status = download_entry.first;

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);
        entry->mutable_status()->CopyFrom(download_status);

        if (download_status.code() == grpc::StatusCode::OK) {
            const auto &data = download_entry.second;
            entry->set_data(data);

            buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
                MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE, 1);

            writeToLocalStorage(digest, data);
            bytes_downloaded += digest.size_bytes();
            ++count;
        }
    }

    if (bytes_downloaded > 0) {
        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_READ,
                                bytes_downloaded);
    }
    BUILDBOX_LOG_INFO("downloaded "
                      << count << " out of " << digests_missing_locally.size()
                      << " digest(s) from the remote cas server");

    return grpc::Status::OK;
}

grpc::Status LocalCasProxyInstance::Write(WriteRequest *request_message,
                                          ServerReader<WriteRequest> &request,
                                          WriteResponse *response)
{
    response->set_committed_size(0);

    const auto buffer_file = d_storage->createTemporaryFile();

    Digest digest_to_write;
    const auto request_status = CasInstance::processWriteRequest(
        request_message, request, &digest_to_write, buffer_file.name());

    if (!request_status.ok()) {
#ifdef BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY
        /* This is made conditional on gRPC's version due to a bug that
         * prevented clients from being notitied that a stream is half-closed:
         * https://github.com/grpc/grpc/pull/22668
         */

        if (request_status.error_code() == grpc::StatusCode::ALREADY_EXISTS) {
            // The blob is already present in the CAS. In that case, according
            // to the REAPI spec:
            // " [...] if another client has already completed the upload
            // [...], the request will terminate immediately with a response
            // whose `committed_size` is the full size of the uploaded file
            // (regardless of how much data was transmitted by the client)"
            response->set_committed_size(digest_to_write.size_bytes());
            return grpc::Status::OK;
        }
#endif

        return request_status;
    }

    // Trying to upload the blob first:
    const auto upload_status = uploadBlob(digest_to_write, buffer_file.name());

    // Upload was successful. We can move the file directly to the CAS,
    // avoiding copies:
    const auto move_status =
        moveToLocalStorage(digest_to_write, buffer_file.name());

    // We assume that the remote will keep the blob for a while, so
    // we'll avoid querying for it in subsequent `FindMissingBlobs()`:
    addToFindMissingBlobsCache(digest_to_write);

    if (move_status.ok()) {
        response->set_committed_size(digest_to_write.size_bytes());
    }

    return move_status;
}

bool LocalCasProxyInstance::hasBlob(const Digest &digest)
{
    return d_storage->hasBlob(digest);
}

google::rpc::Status LocalCasProxyInstance::readBlob(const Digest &digest,
                                                    std::string *data,
                                                    size_t read_offset,
                                                    size_t read_limit)
{
    const google::rpc::Status read_status =
        readFromLocalStorage(digest, data, read_offset, read_limit);
    if (read_status.code() == grpc::StatusCode::OK) {
        return read_status;
    }

    google::rpc::Status status;
    try {
        const std::string fetched_data = d_cas_client->fetchString(digest);

        // While less bytes might be served to clients due to the read_limit,
        // the entire blob is downloaded so we use the full size here
        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_READ,
                                digest.size_bytes());

        if (read_limit == 0) {
            *data = fetched_data.substr(read_offset);
        }
        else {
            *data = fetched_data.substr(read_offset, read_limit);
        }

        // For each blob we successfully downloaded from the remote,
        // increment the count.
        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE, 1);

        status.set_code(grpc::StatusCode::OK);

        writeToLocalStorage(digest, fetched_data);
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in local nor remote CAS");
    }

    return status;
}

google::rpc::Status LocalCasProxyInstance::writeBlob(const Digest &digest,
                                                     const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasProxyInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");

    google::rpc::Status status;

    // Trying to upload the blob first:
    try {
        d_cas_client->upload(data, digest);
    }
    catch (const std::logic_error &) { // Digest does not match the data.
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        return status;
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::UNAVAILABLE);
        return status;
    }
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                            digest.size_bytes());
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE,
                            1);

    // Upload was successful. We now write to the local storage
    // (if needed, LocalCas will skip the write if the file exists.):
    google::rpc::Status write_status = writeToLocalStorage(digest, data);
    status.set_code(write_status.code());

    return status;
}

grpc::Status LocalCasProxyInstance::uploadBlob(const Digest &digest,
                                               const std::string &path) const
{
    const int fd = open(path.c_str(), O_RDONLY);
    if (fd == -1) {
        std::ostringstream error_message;
        error_message << "Could not read intermediate file at \"" << path
                      << "\":" << strerror(errno);
        BUILDBOX_LOG_ERROR(error_message.str());
        return grpc::Status(grpc::StatusCode::INTERNAL, error_message.str());
    }

    grpc::Status upload_status;
    try {
        d_cas_client->upload(fd, digest);
        upload_status = grpc::Status::OK;

        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                                digest.size_bytes());
        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE, 1);
    }
    catch (const std::logic_error &e) { // Digest does not match the data.
        upload_status =
            grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, e.what());
    }
    catch (const std::runtime_error &e) {
        upload_status = grpc::Status(grpc::StatusCode::UNAVAILABLE, e.what());
    }

    close(fd);

    return upload_status;
}

void LocalCasProxyInstance::addToFindMissingBlobsCache(const Digest &digest)
{
    if (d_find_missing_blobs_cache) {
        d_find_missing_blobs_cache->addDigest(digest);
    }
}

std::unordered_map<Digest, grpc::Status>
LocalCasProxyInstance::batchUpdateRemoteCas(
    const BatchUpdateBlobsRequest &request)
{
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    upload_requests.reserve(static_cast<size_t>(request.requests_size()));
    CounterType bytes_uploaded = 0;
    for (const auto &blob : request.requests()) {
        upload_requests.emplace_back(blob.digest(), blob.data());
        bytes_uploaded += blob.digest().size_bytes();
    }

    const std::vector<buildboxcommon::Client::UploadResult>
        blobs_not_uploaded = d_cas_client->uploadBlobs(upload_requests);

    std::unordered_map<Digest, grpc::Status> res;
    for (const buildboxcommon::Client::UploadResult &upload_result :
         blobs_not_uploaded) {
        res.emplace(upload_result.digest, upload_result.status);
        bytes_uploaded -= upload_result.digest.size_bytes();
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                            bytes_uploaded);

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE,
                            upload_requests.size() -
                                blobs_not_uploaded.size());

    return res;
}

grpc::Status LocalCasProxyInstance::findMissingBlobsInRemoteCas(
    const std::vector<Digest> &digests, std::vector<Digest> *missing_digests)
{
    return d_find_missing_blobs_client->findMissingBlobs(digests,
                                                         missing_digests);
}

Status LocalCasProxyInstance::FetchMissingBlobs(
    const FetchMissingBlobsRequest &request,
    FetchMissingBlobsResponse *response)
{
    std::unordered_set<Digest> digests_missing_locally;
    // (Repeated digests in the request will be treated as a single entry.)
    for (const Digest &digest : request.blob_digests()) {
        // Checking the local storage first:
        if (!d_storage->hasBlob(digest)) {
            digests_missing_locally.insert(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        return grpc::Status::OK;
    }

    auto temp_dir = d_storage->createTemporaryDirectory();

    buildboxcommon::Client::DownloadBlobsResult downloaded_data;
    CounterType bytes_downloaded = 0;
    try {
        // Making a request for the blobs that we couldn't find locally:
        downloaded_data = d_cas_client->downloadBlobsToDirectory(
            std::vector<Digest>(digests_missing_locally.cbegin(),
                                digests_missing_locally.cend()),
            temp_dir.name());
    }
    catch (const std::runtime_error &e) {
        const std::string error_message =
            "Error downloading blobs from remote: " + std::string(e.what());
        BUILDBOX_LOG_ERROR(error_message);
        return grpc::Status(grpc::StatusCode::INTERNAL, error_message);
    }

    for (const Digest &digest : digests_missing_locally) {
        const auto digest_path_it = downloaded_data.at(digest.hash());
        const google::rpc::Status download_status = digest_path_it.first;

        // Download failed. We add that to the reponse, which only contains
        // digests that failed:
        if (download_status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(download_status);
            continue;
        }

        // Download suceeded. Add the blob to CAS:
        const std::string &path = digest_path_it.second;
        bytes_downloaded += digest.size_bytes();

        try {
            if (!this->d_storage->hasBlob(digest)) {
                d_storage->moveBlob(digest, path);
            }
            // For each blob we successfully downloaded from the remote,
            // increment the count.
            buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
                MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE, 1);
        }
        catch (const std::runtime_error &e) {
            // We did not add the blob to the CAS. Report that as an error in
            // the response:
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->set_code(grpc::StatusCode::INTERNAL);
            entry->mutable_status()->set_message(
                "Internal error while writing blob to local CAS: " +
                std::string(e.what()));
        }
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_READ,
                            bytes_downloaded);

    return grpc::Status::OK;
}

Status LocalCasProxyInstance::UploadMissingBlobs(
    const UploadMissingBlobsRequest &request,
    UploadMissingBlobsResponse *response)
{
    // Construct std::vector from protobuf digest list
    auto pb_digests = request.blob_digests();
    std::vector<Digest> digests(pb_digests.begin(), pb_digests.end());

    std::vector<Digest> missing_digests;
    const auto find_missing_blobs_status =
        findMissingBlobsInRemoteCas(digests, &missing_digests);
    if (!find_missing_blobs_status.ok()) {
        return find_missing_blobs_status;
    }

    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    // Keep track of the number of bytes we plan to upload
    CounterType bytes_uploaded = 0;
    for (const Digest &digest : missing_digests) {
        google::rpc::Status status;
        status.set_code(grpc::StatusCode::OK);

        try {
            std::string path = d_storage->path(digest);
            upload_requests.emplace_back(
                buildboxcommon::Client::UploadRequest::from_path(digest,
                                                                 path));
            bytes_uploaded += digest.size_bytes();

            // We assume that the remote will keep the blob for a while, so
            // we'll avoid querying for it in subsequent `FindMissingBlobs()`:
            addToFindMissingBlobsCache(digest);
        }
        catch (const BlobNotFoundException &) {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob not found in local cache");
        }
        catch (const std::runtime_error &e) {
            status.set_code(grpc::StatusCode::INTERNAL);
            status.set_message(e.what());
        }

        if (status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(status);
        }
    }

    const std::vector<Client::UploadResult> not_uploaded_blobs =
        d_cas_client->uploadBlobs(upload_requests);

    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        google::rpc::Status status;
        status.set_code(upload_result.status.error_code());
        status.set_message(upload_result.status.error_message());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(upload_result.digest);
        entry->mutable_status()->CopyFrom(status);
        // Unable to upload this blob for some reason, subtract it from
        // the uploaded bytes count
        bytes_uploaded -= upload_result.digest.size_bytes();
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                            bytes_uploaded);
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE,
                            1);

    return grpc::Status::OK;
}

Status
LocalCasProxyInstance::prepareTreeForStaging(const Digest &root_digest) const
{
    // Proxy mode. (We can try and fetch the blobs that might be missing.)
    try {
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_PREPARE);

        fetchTreeMissingBlobs(root_digest);
        return grpc::Status::OK;
    }
    catch (const std::runtime_error &e) {
        const std::string error_message =
            "Error while fetching blobs that are missing locally for staging "
            "directory with root digest \"" +
            toString(root_digest) + "\": " + e.what();
        BUILDBOX_LOG_ERROR(error_message);
        return Status(grpc::StatusCode::FAILED_PRECONDITION, error_message);
    }
}

void LocalCasProxyInstance::fetchTreeMissingBlobs(const Digest &root_digest,
                                                  bool file_blobs) const
{
    const Directory directory = fetchDirectory(root_digest);
    // (This `fetchDirectory()` throws if it fails, in which case we'll abort.)

    const std::vector<Digest> missing_digests =
        digestsMissingFromDirectory(directory, file_blobs);

    if (!missing_digests.empty()) {
        auto temp_dir = d_storage->createTemporaryDirectory();

        const Client::DownloadBlobsResult downloaded_blobs =
            d_cas_client->downloadBlobsToDirectory(missing_digests,
                                                   temp_dir.name());

        CounterType bytes_downloaded = 0;
        CounterType blobs_downloaded = 0;
        for (const Digest &digest : missing_digests) {
            const auto download_result_it = downloaded_blobs.at(digest.hash());
            const google::rpc::Status &status = download_result_it.first;

            if (status.code() == grpc::StatusCode::OK) {
                const std::string &path = download_result_it.second;
                bytes_downloaded += digest.size_bytes();
                blobs_downloaded++;

                if (!this->d_storage->hasBlob(digest)) {
                    this->d_storage->moveBlob(digest, path);
                }
            }
            else {
                // If a single blob is missing we can't stage the directory,
                // aborting:
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error,
                    "Could not fetch missing blob with digest \""
                        << digest << "\": " << status.code() << ": "
                        << status.message());
            }
        }
        // For each blob we successfully downloaded from the remote,
        // increment the count.
        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE, blobs_downloaded);
        buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
            recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_READ,
                                bytes_downloaded);
    }

    // Recursively fetching the missing blobs in each subdirectory:
    for (const DirectoryNode &dir : directory.directories()) {
        fetchTreeMissingBlobs(dir.digest(), file_blobs);
    }
}

Directory
LocalCasProxyInstance::fetchDirectory(const Digest &root_digest) const
{
    Directory directory;
    if (this->d_storage->hasBlob(root_digest) &&
        directory.ParseFromString(*d_storage->readBlob(root_digest))) {
        return directory;
    }

    const std::string directory_blob = d_cas_client->fetchString(root_digest);
    if (!directory.ParseFromString(directory_blob)) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Error parsing fetched Directory with digest "
                                    << toString(root_digest));
    }
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_READ,
                            root_digest.size_bytes());

    d_storage->writeBlob(root_digest, directory.SerializeAsString());
    // For each blob we successfully downloaded from the remote,
    // increment the count.
    buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
        MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE, 1);

    return directory;
}

const Digest LocalCasProxyInstance::UploadAndStore(
    buildboxcommon::digest_string_map *const digest_map, const Tree *t,
    bool bypass_local_cache)
{
    std::vector<Client::UploadRequest> upload_vec;
    // vector size is digest_map + 1 for tree_digest
    upload_vec.reserve(digest_map->size() + 1);

    // If bypass_local_cache is not set, writes digest to localCas
    auto storeDigestsLocally = [&](auto digest, auto content) {
        if (!bypass_local_cache) {
            d_storage->writeBlob(digest, content);
        }
    };

    Directory d;
    CounterType bytes_uploaded = 0;
    for (const auto &it : *digest_map) {
        // Check if string is a directory or file.Directory d;
        if (d.ParseFromString(it.second)) {
            upload_vec.emplace_back(
                Client::UploadRequest(it.first, it.second));
            storeDigestsLocally(it.first, it.second);
        }
        else {
            if (!bypass_local_cache) {
                copyToLocalStorage(it.first, it.second);
            }
            upload_vec.emplace_back(
                Client::UploadRequest::from_path(it.first, it.second));
            bytes_uploaded += it.first.size_bytes();
        }
    }

    Digest tree_digest = Digest();
    if (t != nullptr) {
        // Get and emplace back tree_digest.
        tree_digest = buildboxcommon::make_digest(*t);
        const std::string tree_message_string = t->SerializeAsString();
        upload_vec.emplace_back(
            Client::UploadRequest(tree_digest, tree_message_string));

        storeDigestsLocally(tree_digest, tree_message_string);
    }

    // upload to remote cas
    const auto not_uploaded_blobs = d_cas_client->uploadBlobs(upload_vec);
    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        bytes_uploaded -= upload_result.digest.size_bytes();
    }
    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE,
                            bytes_uploaded);

    buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
        MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE,
        upload_vec.size() - not_uploaded_blobs.size());

    if (not_uploaded_blobs.size() > 0) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Unable to upload "
                                           << not_uploaded_blobs.size()
                                           << " blobs to remote CAS");
    }

    return tree_digest;
}

Status LocalCasProxyInstance::FetchTree(const FetchTreeRequest &request,
                                        FetchTreeResponse *)
{
    buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
        MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_REQUESTS, 1);

    // We might have seen this request recently. If so, don't check the
    // storage:
    if (d_fetch_tree_root_digest_cache.hasRootDigest(
            request.root_digest(), request.fetch_file_blobs())) {
        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_CACHE_HITS, 1);
        return grpc::Status::OK;
    }

    grpc::Status status;
    const auto fetchTreeLambda = [this, &request, &status](const Digest &) {
        try {
            const buildboxcommon::Digest &root_digest = request.root_digest();
            const bool &fetch_files = request.fetch_file_blobs();

            // Unless other thread beat us to it, fetch the tree:
            if (d_fetch_tree_root_digest_cache.hasRootDigest(root_digest,
                                                             fetch_files)) {
                buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
                    MetricNames::
                        COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_CACHE_HITS,
                    1);
            }
            else {
                fetchTreeMissingBlobs(root_digest, fetch_files);
                d_fetch_tree_root_digest_cache.addRootDigest(root_digest,
                                                             fetch_files);
            }

            status = grpc::Status::OK;
        }
        catch (const std::runtime_error &e) {
            const std::string error_message = "Error while fetching blobs for "
                                              "directory with root digest \"" +
                                              toString(request.root_digest()) +
                                              "\": " + e.what();
            BUILDBOX_LOG_ERROR(error_message);
            status = grpc::Status(grpc::StatusCode::NOT_FOUND, error_message);
        }
    };

    // Making sure that there is never more than one call to
    // `fetchTreeMissingBlobs(d)` in-flight for a same digest.
    d_fetch_tree_operation_queue.runExclusively(request.root_digest(),
                                                fetchTreeLambda);

    return status;
}
